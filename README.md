## taxibooking

A simple taxi booking web application made using Python and Django web framework.

This project was created with Python 3.8.2 and Django 3.0.6.
This repository is a django project created using `django-admin startproject taxibooking`.

The repository is mainly _starter code_ for you to get up and running with a Django project and develop upon.

#### Usage

- Create a base directory somewhere in your computer. `cd` inside it, and create and activate a new virtual environment.
- Clone the repository from gitlab. This is your django project which you can `cd` into and run using `python manage.py runserver`.
- `cd` inside the cloned repository, and with the virtual environment activated, install all the dependencies using the command `pip install -r requirements.txt`
- Start the server from the same working directory using the command `python manage.py runserver 8000`.
- Set up dynamoDB locally, and use it from the Django project using `pynamodb`.
