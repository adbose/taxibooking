# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView
from django.views import View
from .forms import RideRequestForm
from .models import Ride
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect


# Create your views here.
class IndexView(TemplateView):
    # This view will return the rides index page
    # for now it will be a list of all rides, in a list view
    def get(self, request, **kwargs):
        return render(request, 'ride_index.html', context=None)


# create a view to fill a simple form and submit a ride request
class RideRequestView(TemplateView):
    def get(self, request):
        form = RideRequestForm()
        # print(form)
        return render(request, template_name='ride_request.html', context={'form': form})

    def post(self, request):
        form = RideRequestForm(data=request.POST)
        if form.is_valid():
            print("form is valid")
            import pdb;pdb.set_trace()
            ride_details = form.cleaned_data
            print(ride_details)
            # create ride_id from ride details and and write to dynamodb

        else:
            print("form is invalid")
            # form = RideRequestForm()
            # return redirect('booking:ride-request')
        # return redirect('booking:ride-details')
        return render(request, template_name='ride_request.html', context={'form': form})


# creates a detail view for a ride and reverses to a unique ride url generated from ride ID as parameter
class RideDetailView(View):
    # this shows the ride details and ride status
    # must generate ride id, and redirect to unique url made using the ride id
    def get(self, request, *args, **kwargs):
        # context = {'ride': ''}
        #fetch data from db
        return render(request, 'ride_details.html')

class RideRequestAPI(View):
    def get(self, request):
        data = {"status": "REQUESTED"}
        return JsonResponse(data)

    # def post(self, request):
    #     form = RideRequestForm(data=request.POST)
    #     if form.is_valid():
    #         print("form is valid")
    #         ride_details = form.cleaned_data
    #         print(ride_details)
    #         # create ride_id from ride details and and write to dynamodb