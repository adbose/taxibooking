# from django.db import models

# Create your models here.
from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute


class Ride(Model):
    """
    A DynamoDB Ride model
    """
    class Meta:
        table_name = "dynamodb-rides-local"
        host = "http://localhost:8500"
    ride_id = UnicodeAttribute(hash_key=True)
    time = UnicodeAttribute(range_key=True)
    status = UnicodeAttribute()

