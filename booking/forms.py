from django import forms

class RideRequestForm(forms.Form):
    username = forms.CharField()
    origin = forms.CharField()
    destination = forms.CharField()
    time = forms.CharField()