from django.urls import path
from .views import IndexView, RideRequestView, RideDetailView, RideRequestAPI

# TODO: Add ur names/namespaces in the path
app_name = "booking"
urlpatterns = [
    path('', IndexView.as_view(), name='ride-index'),  # ride list view
    path('new/', RideRequestView.as_view(), name='ride-request'),  # Add this /about/ route
    # path('<int:ride_id>/', RideDetailView.as_view()),
    path('details/', RideDetailView.as_view(), name='ride-details'),
    path('api/ride-request/', RideRequestAPI.as_view(), name='ride-request-api')
]
