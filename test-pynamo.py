# from django.db import models

# Create your models here.
from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute
import json
import requests

class Ride(Model):
    """
    A DynamoDB Ride model
    """

    class Meta:
        read_capacity_units = 2
        write_capacity_units = 2
        aws_access_key_id = "anything"
        aws_secret_access_key = "anything"
        table_name = "dynamodb-test-rides"
        host = "http://localhost:8500"

    ride_id = UnicodeAttribute(hash_key=True)
    time = UnicodeAttribute(range_key=True)
    status = UnicodeAttribute(null=True)


def main():
    print("1. Create table")
    print("2. Fetch table")
    print("3. Delete table")
    n = int(input("Enter your choice: "))
    if n == 1:
        print("Proceeding to create table")
        if not Ride.exists():
            Ride.create_table(wait=True)
        ride = Ride("ride1", "today", "REQUESTED")
        ride.save()
        print(ride)
        print(type(ride))

    if n == 2:
        # Scan
        for item in Ride.scan():
            print(item)

        # Get
        ride_item = Ride.get('ride1', 'today')
        print(ride_item)

        # Query
        for item in Ride.query('ride1'):
            print(item)
            print(item.status)

    if n == 3:
        Ride.delete_table()


def main3():
    response = requests.get('https://google.com')
    print(response)
    print(response.json())


if __name__ == '__main__':
    main()